/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/*
 * Seen Marketplace
 */
angular.module('seen-marketplace', [ 'seen-core' ])

/*
 * Factories - SPA - Template
 * 
 * Supper tenant factories: - Tenant - Configuration
 */

/**
 * @ngdoc Factories
 * @name MarketplaceSpa
 * @description اطلاعات یک نرم افزار از مخزن را تعیین می‌کند.
 */
.factory('MarketplaceSpa', seen.factory({
	url : '/api/v2/marketplace/spas',
	resources : [ {
		name : 'File',
		type : 'binary',
		url : '/file'
	}
//	, {
//		name : 'PossibleTransition',
//		factory : 'SpaTransition',
//		type : 'collection',
//		url : '/possible-transitions'
//	}, {
//		name : 'Transition',
//		factory : 'SpaTransition',
//		type : 'collection',
//		url : '/transitions'
//	} 
	]
}))

/**
 * @ngdoc Services
 * @name $marketplace
 * @description Marketplace service
 * 
 * Manages SPAs on a repositories
 */
.service('$marketplace', seen.service({
	resources : [ {
		name : 'Spa',
		factory : 'MarketplaceSpa',
		type : 'collection',
		url : '/api/v2/marketplace/spas'
	} ]
}));
