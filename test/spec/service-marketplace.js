  /*
   * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
   * 
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   * 
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   * 
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   * SOFTWARE.
   */
  'use strict';
  angular.module('TestModule', ['seen-test', 'seen-marketplace']);

  describe('Marketplace service', function () {
      var $marketplace;
      var $test;

      beforeEach(function () {
          module('TestModule');
          inject(function (_$marketplace_, _$test_) {
              $marketplace = _$marketplace_;
              $test = _$test_;
          });
      });


      var types = [{
              name: 'Spa',
              url: '/api/v2/marketplace/spas'
          }];

      for (var i = 0; i < types.length; i++) {
          var type = types[i];
          // Function test
          it('should contains ' + type.name + ' functions', function () {
              expect($test).not.toBeNull();
              $test.serviceCollectionFunction($marketplace, type);
          });
          // Get items
          it('should call GET:' + type.url + ' to list items form collectinos', function (done) {
              $test.serviceGetList($marketplace, type, done);
          });
          // delete items form collection
          it('should call DELETE:' + type.url + ' to delete items form collectinos', function (done) {
              $test.serviceDeleteList($marketplace, type, done);
          });
          // PUT items form collection
          it('should call PUT:' + type.url + ' to add items form collectinos', function (done) {
              $test.servicePutList($marketplace, type, done);
          });

          // Get an item with id
          it('should call GET:' + type.url + '/1 to an item form collectinos', function (done) {
              $test.serviceGetItem($marketplace, type, done);
          });
          // Get an item with id
          it('should call PUT:' + type.url + ' to an item form collectinos', function (done) {
              $test.servicePutItem($marketplace, type, done);
          });
      }
  });
