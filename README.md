# Seen Marketplace

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-marketplace/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-marketplace/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a270f90470994eed83a6ecc0ebd22d97)](https://www.codacy.com/app/seen/seen-marketplace?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-marketplace&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-marketplace/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-marketplace/commits/develop)